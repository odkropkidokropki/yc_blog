<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="NRGNetwork is a responsive and modern social network WordPress theme allowing you to create your very own social network website in minutes.">
    <meta name="format-detection" content="telephone=no" />
     <link rel="shortcut icon" href="http://i.imgur.com/pO3yLoo.png" type="image/x-icon">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php if (NRGnetwork_Std::get_mod('loader_off') != '1'): ?>
        <!-- THE LOADER -->
        <div class="be-loader">
            <div class="spinner">
                <?php
                    $loader_logo = NRGnetwork_Std::get_mod('loader_logo');
                    $h_logo = NRGnetwork_Std::get_mod('logo');
                    $loader_width = NRGnetwork_Std::get_mod('loader_logo_with');
                    if( !empty($loader_logo) ){
                        print '<img src="'.esc_url($loader_logo).'" alt="'.get_bloginfo('name').'">';
                    } else{
                        print '<img src="'.get_template_directory_uri().'/img/logo-loader.png"  alt="'.get_bloginfo('name').'">';
                    }
                ?>
                <p class="circle">
                    <span class="ouro">
                        <span class="left"><span class="anim"></span></span>
                        <span class="right"><span class="anim"></span></span>
                    </span>
                </p>
            </div>
        </div>
    <?php endif; ?>
    <!-- THE HEADER -->
    <header>
        <div class="container-fluid custom-container">
            <div class="row no_row row-header">
                <div class="brand-be">
                     <a href="<?php print esc_url(home_url('/')); ?>">
                         <?php if( !empty($loader_logo) ){
                            print '<img src="'.esc_url($h_logo).'" class="logo-c active be_logo img-responsive"/>';
                         } else {
                            print '<img class="logo-c active be_logo"  src="https://yourcrochet.com/designers/wp-content/uploads/2020/06/YC-logo-n.png" alt="logo">';
                         }
                         ?>                    
                    </a>
                </div>
                <div class="header-menu-block">
                    
                    <button class="cmn-toggle-switch cmn-toggle-switch__htx"><span></span></button>
                    <?php
                        $nav_menu=wp_nav_menu( array(
                            'menu_id'           => 'one',
                            'menu_class'        => 'header-menu',
                            'theme_location'    => 'primary',
                            'container'         => '',
                            'fallback_cb'       => 'nrgnetwork_primary_callback',
                            'echo'              =>false

                        ) );
                    $home_link=home_url();
                    $nav_menu=str_replace('href="/','href="'.$home_link.'/',$nav_menu);
                    print $nav_menu;
                    ?>
                </div>
                <div class="login-header-block">
                    <div class="login_block">
                        <?php if( !is_user_logged_in() ): ?>
                            <a class="btn-login btn color-1 size-2 hover-2" href="javascript:;">
                                <i class="fa fa-user"></i>
                                <?php esc_html_e('Login', 'nrgnetwork'); ?>
                            </a>
                        <?php
                            else:
                                get_template_part('tpl', 'author-header');
                            endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </header>