<form method="get" action="<?php print esc_url(home_url('/')); ?>" class="input-search">
    <input type="text" required placeholder="<?php esc_html_e('Enter Keyword', 'nrgnetwork'); ?>" name="s">
    <i class="fa fa-search"></i>
    <input type="submit" value="">
</form>